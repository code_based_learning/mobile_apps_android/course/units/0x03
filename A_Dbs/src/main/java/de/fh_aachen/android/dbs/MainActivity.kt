// (C) 2024 A.Voß, a.voss@fh-aachen.de, apps@codebasedlearning.dev

package de.fh_aachen.android.dbs

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.MenuAnchorType
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import de.fh_aachen.android.dbs.ui.theme.MyAppTheme

import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.lifecycle.viewmodel.compose.viewModel

import de.fh_aachen.android.dbs.database.IntCategoryEntity
import de.fh_aachen.android.dbs.database.IntProductEntity
import de.fh_aachen.android.dbs.model.DbsViewModel
import de.fh_aachen.android.dbs.ui.theme.BackgroundImage
import de.fh_aachen.android.dbs.ui.theme.CircularIconButton
import java.util.UUID
import kotlin.random.Random

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MyAppTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    MainScreen(modifier = Modifier.padding(innerPadding))
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryComboBox(modifier: Modifier = Modifier, onItemSelected: (IntCategoryEntity) -> Unit) {
    var expanded by remember { mutableStateOf(false) }          // controls dropdown visibility
    var selectedOption by remember { mutableStateOf("") }       // holds selected option
    val viewModel: DbsViewModel = viewModel()
    val categories by viewModel.categories.collectAsState()     // collect categories as state

    ExposedDropdownMenuBox(expanded = expanded, modifier = modifier.padding(8.dp),
        onExpandedChange = { expanded = !expanded }
    ) {
        TextField(
            value = selectedOption, onValueChange = {}, readOnly = true,
            label = { Text("Select a category") },
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
            modifier = Modifier
                .fillMaxWidth()
                .menuAnchor(MenuAnchorType.PrimaryNotEditable),
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
        )
        ExposedDropdownMenu(expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            categories.forEach { item ->
                DropdownMenuItem(
                    text = { Text(item.name) },
                    onClick = {
                        selectedOption = item.name
                        expanded = false
                        onItemSelected(item)
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductComboBox(modifier: Modifier = Modifier, onItemSelected: (IntProductEntity) -> Unit) {
    var expanded by remember { mutableStateOf(false) }          // controls dropdown visibility
    var selectedOption by remember { mutableStateOf("") }       // holds selected option
    val viewModel: DbsViewModel = viewModel()
    val products by viewModel.products.collectAsState()            // collect products as state

    ExposedDropdownMenuBox(expanded = expanded, modifier = modifier.padding(8.dp),
        onExpandedChange = { expanded = !expanded }
    ) {
        TextField(
            value = selectedOption, onValueChange = {}, readOnly = true,
            label = { Text("Select a product") },
            trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
            modifier = Modifier
                .fillMaxWidth()
                .menuAnchor(MenuAnchorType.PrimaryNotEditable),
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
        )
        ExposedDropdownMenu(expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            products.forEach { item ->
                DropdownMenuItem(
                    text = { Text(item.name) },
                    onClick = {
                        selectedOption = item.name
                        expanded = false
                        onItemSelected(item)
                    }
                )
            }
        }
    }
}

@Composable
fun MainScreen(modifier: Modifier) {
    val viewModel: DbsViewModel = viewModel()

    var catId by remember { mutableStateOf(UUID.randomUUID()) }
    var catName by remember { mutableStateOf("") }
    var productId by remember { mutableStateOf(UUID.randomUUID()) }
    var productName by remember { mutableStateOf("") }

    Box(modifier = modifier.fillMaxSize()) {
        BackgroundImage(id = R.drawable.fruits)
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.TopCenter) {
            Column(modifier = Modifier.fillMaxSize()) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, start = 8.dp, end = 8.dp)
                        .background(Color(0x88AA0000)),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    CategoryComboBox(modifier = Modifier.weight(0.7f)) { selectedItem ->
                        viewModel.getAllProductsFromCategory(selectedItem.id)
                        catId = selectedItem.id
                        catName = selectedItem.name
                    }
                    Row(modifier = Modifier.weight(0.3f), horizontalArrangement = Arrangement.End) {
                        CircularIconButton(
                            iconResourceId = R.drawable.baseline_clear_all_24,
                            contentDescription = "Reset"
                        ) {
                            viewModel.resetData()
                        }
                        CircularIconButton(
                            iconResourceId = R.drawable.baseline_sync_24,
                            contentDescription = "Sync"
                        ) {
                            viewModel.syncWithExternalData()
                        }
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 8.dp, start = 8.dp, end = 8.dp)
                        .background(Color(0x88AA0000)),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    ProductComboBox(modifier = Modifier.weight(0.7f)) { selectedItem ->
                        productId = selectedItem.id
                        productName = selectedItem.name
                    }
                    Row(modifier = Modifier.weight(0.3f), horizontalArrangement = Arrangement.End) {
                        CircularIconButton(
                            iconResourceId = R.drawable.baseline_add_24,
                            contentDescription = "Plus"
                        ) {
                            productName =
                                "New Product no ${Random.nextInt(from = 1000, until = 9999)}"
                            viewModel.addProduct(productName, catId)
                        }
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, start = 8.dp, end = 8.dp)
                        .background(Color(0xAA00AAFF)),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = "Category",
                        modifier = Modifier
                            .weight(0.3f)
                            .padding(8.dp),
                        textAlign = TextAlign.End,
                        color = Color.White
                    )
                    TextField(
                        value = catName, readOnly = true,
                        onValueChange = { },
                        modifier = Modifier
                            .weight(0.5f)
                            .padding(8.dp),
                    )
                    Spacer(Modifier.width(48.dp))
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp, end = 8.dp)
                        .background(Color(0xAA00AAFF)),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = "Product",
                        modifier = Modifier
                            .weight(0.3f)
                            .padding(8.dp),
                        textAlign = TextAlign.End,
                        color = Color.White
                    )
                    TextField(
                        value = productName,
                        onValueChange = { newText -> productName = newText },
                        modifier = Modifier
                            .weight(0.5f)
                            .padding(8.dp),
                    )
                    CircularIconButton(
                        iconResourceId = R.drawable.baseline_check_24,
                        contentDescription = "Plus"
                    ) {
                        viewModel.updateProductLabel(productId, productName)
                    }
                }
            }
        }
    }
}

