// (C) 2024 A.Voß, a.voss@fh-aachen.de, apps@codebasedlearning.dev

package de.fh_aachen.android.dbs.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.fh_aachen.android.dbs.DbsApplication
import de.fh_aachen.android.dbs.database.IntCategoryDao
import de.fh_aachen.android.dbs.database.IntCategoryEntity
import de.fh_aachen.android.dbs.database.IntProductDao
import de.fh_aachen.android.dbs.database.IntProductEntity
import de.fh_aachen.android.dbs.database.LocalSQLiteDatabase
import de.fh_aachen.android.dbs.database.syncWithFreeSql
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.UUID

class DataRepository(private val localDatabase: LocalSQLiteDatabase) {
    private val categoryDao: IntCategoryDao = localDatabase.categoryDao()
    private val productDao: IntProductDao = localDatabase.productDao()

    fun getAllCategories() = categoryDao.getAllCategories()

    fun getAllProductsFromCategory(categoryId: UUID) = productDao.getItemsForCategory(categoryId)

    suspend fun addProduct(product: IntProductEntity) = productDao.insert(product)

    suspend fun updateProductLabel(categoryId: UUID, newLabel: String) =
        productDao.updateItemLabel(categoryId, newLabel)

    suspend fun syncWithExternalDatabase() = syncWithFreeSql()

    suspend fun resetLocalDatabase() {
        localDatabase.clearAllTables()
        val catFrozen = IntCategoryEntity(name = "Frozen Goods")
        val itemPizza1 = IntProductEntity(name = "Cheese Pizza", categoryId = catFrozen.id)
        val itemPizza2 = IntProductEntity(name = "Spinach Pizza", categoryId = catFrozen.id)
        categoryDao.insert(catFrozen)
        productDao.insert(itemPizza1)
        productDao.insert(itemPizza2)
        val catFruits = IntCategoryEntity(name = "Fruits, Vegetables")
        val itemFruit1 = IntProductEntity(name = "Bananas", categoryId = catFruits.id)
        val itemFruit2 = IntProductEntity(name = "Carrots", categoryId = catFruits.id)
        val itemFruit3 = IntProductEntity(name = "Onions", categoryId = catFruits.id)
        categoryDao.insert(catFruits)
        productDao.insert(itemFruit1)
        productDao.insert(itemFruit2)
        productDao.insert(itemFruit3)
        val catDrinks = IntCategoryEntity(name = "Drinks")
        val itemDrink1 = IntProductEntity(name = "Water", categoryId = catDrinks.id)
        val itemDrink2 = IntProductEntity(name = "Coke", categoryId = catDrinks.id)
        val itemDrink3 = IntProductEntity(name = "Beer", categoryId = catDrinks.id)
        val itemDrink4 = IntProductEntity(name = "Wine", categoryId = catDrinks.id)
        categoryDao.insert(catDrinks)
        productDao.insert(itemDrink1)
        productDao.insert(itemDrink2)
        productDao.insert(itemDrink3)
        productDao.insert(itemDrink4)
    }
}

// if you need  the application context you can inherit from AndroidViewModel
class DbsViewModel() : ViewModel() {
    // feel free to use a service locator or DI framework
    private val repository = DbsApplication.instance.dataRepository

    // StateFlow to hold the list of categories
    val categories: StateFlow<List<IntCategoryEntity>> = repository.getAllCategories()
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), emptyList())

    // Mutable StateFlow for products of a selected category
    private val _products = MutableStateFlow<List<IntProductEntity>>(emptyList())
    val products: StateFlow<List<IntProductEntity>> = _products.asStateFlow()

    // fetch products for a specific category
    fun getAllProductsFromCategory(categoryId: UUID) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getAllProductsFromCategory(categoryId).collect { itemList ->
                _products.value = itemList
            }
        }
    }

    fun addProduct(name: String, categoryId: UUID) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addProduct(IntProductEntity(name = name, categoryId = categoryId))
        }
    }

    fun updateProductLabel(categoryId: UUID, newLabel: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateProductLabel(categoryId, newLabel)
        }
    }

    fun resetData() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.resetLocalDatabase()
        }
    }

    fun syncWithExternalData() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.syncWithExternalDatabase()
        }
    }
}
