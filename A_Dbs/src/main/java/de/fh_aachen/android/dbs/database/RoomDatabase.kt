// (C) 2024 A.Voß, a.voss@fh-aachen.de, apps@codebasedlearning.dev

package de.fh_aachen.android.dbs.database

import android.content.Context
import androidx.room.*
import kotlinx.coroutines.flow.Flow
import java.util.UUID
import androidx.room.TypeConverter

@Entity(tableName = "category")
data class IntCategoryEntity(
    // @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @PrimaryKey val id: UUID = UUID.randomUUID(),
    val name: String
)

@Entity(
    tableName = "product",
    foreignKeys = [ForeignKey(
        entity = IntCategoryEntity::class,
        parentColumns = ["id"],
        childColumns = ["categoryId"],
        onDelete = ForeignKey.CASCADE
    )]
)
data class IntProductEntity(
    @PrimaryKey val id: UUID = UUID.randomUUID(),
    val name: String,
    val categoryId: UUID
)

@Dao
interface IntCategoryDao {
    @Insert
    suspend fun insert(category: IntCategoryEntity)

    @Query("SELECT * FROM category")
    fun getAllCategories(): Flow<List<IntCategoryEntity>>

    // @Query("SELECT * FROM category WHERE id = :id")
    // fun getCategoryById(id: UUID): Flow<IntCategoryEntity?>
}

@Dao
interface IntProductDao {
    @Insert
    suspend fun insert(item: IntProductEntity)

    @Query("SELECT * FROM product WHERE categoryId = :categoryId")
    fun getItemsForCategory(categoryId: UUID): Flow<List<IntProductEntity>>

    @Query("UPDATE product SET name = :newName WHERE id = :id")
    suspend fun updateItemLabel(id: UUID, newName: String)
}

class Converters {
    @TypeConverter
    fun fromUUID(uuid: UUID?): String? {
        return uuid?.toString()
    }

    @TypeConverter
    fun toUUID(uuid: String?): UUID? {
        return uuid?.let { UUID.fromString(it) }
    }
}

@Database(entities = [IntCategoryEntity::class, IntProductEntity::class], version = 7)
@TypeConverters(Converters::class)
abstract class LocalSQLiteDatabase : RoomDatabase() {
    abstract fun categoryDao(): IntCategoryDao
    abstract fun productDao(): IntProductDao

    companion object {
        @Volatile
        private var instance: LocalSQLiteDatabase? = null

        fun getDatabase(context: Context): LocalSQLiteDatabase {
            return instance ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    LocalSQLiteDatabase::class.java,
                    "product_category_db"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                Companion.instance = instance
                instance
            }
        }
    }
}

/*
You can define migration strategies, e.g.

val MIGRATION_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        // Example: Add a new column to the Category table
        database.execSQL("ALTER TABLE Category ADD COLUMN description TEXT")
    }
}

val database = Room.databaseBuilder(
    context,
    FreeSqlDatabase::class.java,
    "product_category_db"
)
    .addMigrations(MIGRATION_1_2)
    .build()
*/
