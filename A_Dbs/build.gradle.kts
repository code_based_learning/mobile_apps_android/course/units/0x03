import java.util.Properties

plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.compose)

    alias(libs.plugins.kotlin.kapt)
}

android {
    val properties = Properties().apply {
        load(project.rootProject.file("local.properties").inputStream())
    }
    val freeSqlUrl: String = properties.getProperty("freesql_url") ?: ""
    val freeSqlPort: String = properties.getProperty("freesql_port") ?: ""
    val freeSqlDatabase: String = properties.getProperty("freesql_database") ?: ""
    val freeSqlDriver: String = properties.getProperty("freesql_driver") ?: ""
    val freeSqlUser: String = properties.getProperty("freesql_user") ?: ""
    val freeSqlPassword: String = properties.getProperty("freesql_password") ?: ""

    namespace = "de.fh_aachen.android.dbs"
    compileSdk = 35

    defaultConfig {
        applicationId = "de.fh_aachen.android.dbs"
        minSdk = 27     // Oreo 8.1
        targetSdk = 35  // Android 15
        versionCode = 1
        versionName = "1.0"

        buildConfigField("String", "freeSqlUrl", "\"$freeSqlUrl\"")
        buildConfigField("String", "freeSqlPort", "\"$freeSqlPort\"")
        buildConfigField("String", "freeSqlDatabase", "\"$freeSqlDatabase\"")
        buildConfigField("String", "freeSqlDriver", "\"$freeSqlDriver\"")
        buildConfigField("String", "freeSqlUser", "\"$freeSqlUser\"")
        buildConfigField("String", "freeSqlPassword", "\"$freeSqlPassword\"")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17 // (std) VERSION_1_8 or VERSION_11
        targetCompatibility = JavaVersion.VERSION_17 // (std) VERSION_1_8 or VERSION_11
    }
    kotlinOptions {
        jvmTarget = "17" //(std) "1.8" or "11"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(platform(libs.androidx.compose.bom))
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material3)
    implementation(libs.androidx.lifecycle.viewmodel.compose)
    implementation(libs.androidx.room.runtime)
    implementation(libs.androidx.navigation.compose)
    kapt(libs.androidx.room.compiler)
    implementation(libs.androidx.room.ktx)
    implementation(libs.exposed.core)
    implementation(libs.exposed.dao)
    implementation(libs.exposed.jdbc)
    implementation(libs.exposed.java.time)
    implementation(libs.mysql.connector.java)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.androidx.compose.bom))
    androidTestImplementation(libs.androidx.ui.test.junit4)
    debugImplementation(libs.androidx.ui.tooling)
    debugImplementation(libs.androidx.ui.test.manifest)
}
