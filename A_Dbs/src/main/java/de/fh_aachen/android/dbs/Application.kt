// (C) 2024 A.Voß, a.voss@fh-aachen.de, apps@codebasedlearning.dev

package de.fh_aachen.android.dbs

import android.app.Application
import de.fh_aachen.android.dbs.database.LocalSQLiteDatabase
import de.fh_aachen.android.dbs.model.DataRepository

// Do not forget to add this to the AndroidManifest.xml file ('android:name').

class DbsApplication : Application() {

    // feel free to use a service locator or DI framework
    companion object {
        lateinit var instance: DbsApplication
    }

    private val localDatabase: LocalSQLiteDatabase by lazy { LocalSQLiteDatabase.getDatabase(this) }
    val dataRepository: DataRepository by lazy { DataRepository(localDatabase) }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
