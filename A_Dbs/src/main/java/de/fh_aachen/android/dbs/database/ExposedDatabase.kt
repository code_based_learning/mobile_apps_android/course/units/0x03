// (C) 2024 A.Voß, a.voss@fh-aachen.de, apps@codebasedlearning.dev

package de.fh_aachen.android.dbs.database

import android.util.Log
import de.fh_aachen.android.dbs.BuildConfig
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.IOException
import java.sql.SQLException
import java.time.LocalDateTime

/*
Here we use the 'exposed' framework to access an external MySQL-database hosted at freesqldatabase.

Warning: It’s technically possible to connect directly to a MySQL or MariaDB database from
an Android app, but it’s not advisable due to several concerns:
  - Security: Directly exposing your database to the public (like mobile clients) is risky.
    Even if you secure your database credentials in the app, there’s a chance they could be
    extracted, potentially compromising your data.
  - Performance: Mobile networks can be inconsistent. Direct database connections may not handle
    intermittent connectivity well, which could result in incomplete transactions or data corruption.
  - Data Integrity and Validation: A backend can enforce rules, validation, and business logic
    before data reaches the database. With direct access, all validation would need to be handled
    on the client side, which is less secure and reliable.
  - Database Drivers: MySQL and MariaDB drivers are designed for server-side Java and may not
    be optimized for Android, which could lead to compatibility issues or memory overhead
    on mobile devices.

    If you’re experimenting or working on a small, internal project where security and scalability
    are less of a concern, you could technically connect directly to a database.
    In general, using a server, e.g. with a REST interface, is a safer approach.
 */

private const val TAG = "ExposedSql"

private object ExtCategoryTable : Table("category") {
    // val id = integer("id").autoIncrement()
    // val id = uuid("id")
    val id = varchar("id", length = 36)
    val label = varchar("label", length = 255)
    val lastModified = datetime("last_modified") // .default(LocalDateTime.now())
    val status = integer("status")
    override val primaryKey = PrimaryKey(id)
}

private data class ExtCategoryEntity(
    val id: String,
    val label: String,
    val lastModified: LocalDateTime,
    val status: Int
)

private object ExtProductTable : Table("product") {
    val id = varchar("id", length = 36)
    val name = varchar("name", length = 255)
    val comment = varchar("comment", length = 255)
    val categoryId = varchar("category_id", length = 36)
    val lastModified = datetime("last_modified")
    val status = integer("status")
    override val primaryKey = PrimaryKey(id)
}

private data class ExtProductEntity(
    val id: String,
    val name: String,
    val comment: String,
    val categoryId: String,
    val lastModified: LocalDateTime,
    val status: Int
)

fun syncWithFreeSql() {
    // old code: Class.forName(JDBC_DRIVER).newInstance();
    val freeSqlUrl = BuildConfig.freeSqlUrl
    val freeSqlPort = BuildConfig.freeSqlPort
    val freeSqlDatabase = BuildConfig.freeSqlDatabase
    val freeSqlDriver =
        BuildConfig.freeSqlDriver   // mysql 5: com.mysql.jdbc.Driver; mysql 8: com.mysql.cj.jdbc.Driver
    val freeSqlUser = BuildConfig.freeSqlUser
    val freeSqlPassword = BuildConfig.freeSqlPassword
    try {
        val database = Database.connect(
            "jdbc:mysql://$freeSqlUrl:$freeSqlPort/$freeSqlDatabase?useSSL=false",
            driver = freeSqlDriver,
            user = freeSqlUser,
            password = freeSqlPassword
        )
        Log.i(TAG, "connected: ${database.version}")

        printAllCategories()
        printAllProducts()
        printAllProductsWithCategory()
        // do the sync...
    } catch (e: SQLException) {
        Log.e(TAG, "sql error: ${e.message}")
    } catch (e: IOException) {
        Log.e(TAG, "io error: ${e.message}")
    } catch (e: Exception) {
        Log.e(TAG, "some error: ${e.message}")
    }
}

private fun printAllCategories() {
    transaction {
        ExtCategoryTable.selectAll()
            .map { row ->
                ExtCategoryEntity(
                    id = row[ExtCategoryTable.id],
                    label = row[ExtCategoryTable.label],
                    lastModified = row[ExtCategoryTable.lastModified],
                    status = row[ExtCategoryTable.status]
                )
            }.forEach { cat ->
                Log.v(TAG, "raw category: $cat")
            }
    }
}

private fun printAllProducts() {
    transaction {
        ExtProductTable.selectAll()
            .map { row ->
                ExtProductEntity(
                    id = row[ExtProductTable.id],
                    name = row[ExtProductTable.name],
                    comment = row[ExtProductTable.comment],
                    categoryId = row[ExtProductTable.categoryId],
                    lastModified = row[ExtProductTable.lastModified],
                    status = row[ExtProductTable.status]
                )
            }.forEach { product ->
                Log.v(TAG, "raw product: $product")
            }
    }
}

private fun printAllProductsWithCategory() {
    transaction {
        ExtProductTable.join(
            ExtCategoryTable,
            JoinType.INNER,
            additionalConstraint = { ExtProductTable.categoryId eq ExtCategoryTable.id }
        ).selectAll().forEach { row ->
            val name = row[ExtProductTable.name]
            val label = row[ExtCategoryTable.label]
            Log.v(TAG, "join: product '$name' belongs to '$label'")
        }
    }
}
